
import { Col, Row } from "reactstrap";
import like from '../../../assets/images/like.png'
function OutputContent () {
    
        let {outputMessageProps,likeDisplayProps} = this.props
        return (
        <>
            <Row className="mt-2" >
                 {outputMessageProps.map((element,index) => {
                    return <p key={index}>{element}</p>
                 })}
            </Row>
            
            <Row className="mt-2">
                <Col >
                {likeDisplayProps ? <img src={like}  width ='200px;'></img> : <></> }
                </Col>
            </Row>
            
        </>
        )
    
}

export default OutputContent;