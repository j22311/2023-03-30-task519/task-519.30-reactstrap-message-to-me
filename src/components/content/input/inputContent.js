import { Component } from "react";
import { Button, Col, Input, Label, Row } from "reactstrap";

function InputContent () {
    onInputchangeHandler = (event) => {
        console.log("nhập message...");
        console.log(event.target.value); 
        let value = event.target.value;

        this.props.inputMessageChangHandleProps(value);
    }
    onButtonClickHandler = () => {
        console.log("Ấn nút gửi thông điệp ")
        this.props.outputMessageChangHandleProps();
    }
    
        return (
        <>
            
            <Row className="mt-2">
                <Label>Message cho 12 tháng tới</Label>
            </Row>
           
            <Row>
                <Col>
                    <Input value={this.props.inputMessageProps}  placeholder="nhập email ở đây" onChange={this.onInputchangeHandler} style={{width: "600px",margin: "0 auto"}}/>
                </Col>
            </Row>
            
            <Row className="mt-2">
                <Col>
                    <Button color="info" onClick={this.onButtonClickHandler}> Gửi thông điệp</Button>
                </Col>
            </Row>
            
            </>
        )
    
}

export default InputContent;